import os
import time

os.system('cls')
start, stop = 1, 24

while True:
    for i in range(13):
        with open("earth.md", "r") as f:
            frames = [f.readlines()[start:stop]]
            for frame in frames:
                print('\x1b[36m'.join(frame))
                time.sleep(1)
                os.system('cls')
            start += 24
            stop += 24
    start, stop = 1, 24
