from copy import deepcopy
from random import randint
from timeit import default_timer


# Функция нахождения определителя матрицы
def determinant(matr, n):
    if n == 1:
        return matr[0][0]
    elif n == 2:
        return matr[0][0] * matr[1][1] - matr[1][0] * matr[0][1]
    else:
        s = 0
        for i in range(n):
            matr_1 = deepcopy(matr)
            matr_1.pop(0)
            for j in range(n - 1):
                matr_1[j].pop(i)
            s += (-1) ** i * matr[0][i] * determinant(matr_1, n - 1)
        return s


# Функция для решения системы уравнений методом Крамера
def method_cramer(coefs, free_coefs, num, d):
    value = []
    for i in range(num):  # Для каждой переменной
        line = deepcopy(coefs)
        for j in range(num):
            # Заменяем столбец с коэффициентами при переменной на свободные
            line[j][i] = free_coefs[j]
        # Находим определитель матрицы с заменёнными коэффициентами
        line_det = determinant(line, num)
        value.append(line_det / d)
    return value


# Функция для решения системы уравнений методом Гаусса
def method_gauss(matr, num):
    for i in range(0, num):
        maximum_element = abs(matr[i][i])
        maximum_range = i
        for j in range(i + 1, num):
            if abs(matr[j][i]) > maximum_element:
                maximum_element = abs(matr[j][i])
                maximum_range = j

        for j in range(i, num + 1):
            matr[maximum_range][j], matr[i][j] = matr[i][j], matr[maximum_range][j]

        for j in range(i + 1, num):
            result = - (matr[j][i] / matr[i][i])
            for k in range(i, num + 1):
                if i == k:
                    matr[j][k] = 0
                else:
                    matr[j][k] += result * matr[i][k]

    solution = [0 for elem in range(num)]
    for i in range(num - 1, -1, -1):
        solution[i] = matr[i][num] / matr[i][i]
        for j in range(i - 1, -1, -1):
            matr[j][num] -= matr[j][i] * solution[i]
    return solution


# Создание массивов для хранения данных
coefficients, free_coefficients = [], []

# Размерность системы уравнений
min_num = int(input('Сколько уравнений?(min) '))
max_num = int(input('Сколько уравнений?(max) '))
for num in range(min_num, max_num):
    for i in range(num):
        coefficients.append([])
        for j in range(num):
            # Записываем в массив коэффициенты
            coefficients[i].append(randint(-10, 10))
        # Записываем в массив свободные коэффициенты
        free_coefficients.append(randint(-10, 10))

    # Определитель матрицы
    det = determinant(coefficients, num)

    ''' Запись времени решения системы уравнений
     и объема занимаемой памяти в файл (метод Крамера)'''

    if det == 0:
        print("Определитель равен нулю")
    else:
        start_time = default_timer()
        result_cramer = method_cramer(coefficients, free_coefficients, num, det)
        with open('time_cramer.txt', 'a') as file:
            file.write(str(default_timer() - start_time).replace('.', ',') + '\n')
        print('Ответ уравнений методом Крамера:', result_cramer)

    ''' Запись времени решения системы уравнений
     и объема занимаемой памяти в файл (метод Гаусса)'''

    matrix = [[randint(0, 9) for i in range(num + 1)] for j in range(num)]
    start_time = default_timer()
    result_gauss = method_gauss(matrix, num)
    with open('time_gauss.txt', 'a') as file:
        file.write(str(default_timer() - start_time).replace('.', ',') + '\n')
    print('Ответ уравнений методом Гаусса:', result_gauss)
