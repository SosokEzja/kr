from random import randint, uniform
from timeit import default_timer
import matplotlib.pyplot as plt


def vectors(v_x, v_y, c):  # Создание вектора z и определение его длины
    scal = [c * vi for vi in v_x]  # Умножаем вектор х на скаляр
    z = [vi + wi for vi, wi in zip(scal, v_y)]
    return len(z)


def plot(n_int, n_fl, time1, time2):  # Построение графика зависимости времени подсчета от длины вектора
    fig, ax = plt.subplots()
    plt.plot(n_int, time1, marker='o', label='int')
    plt.plot(n_fl, time2, marker='o', label='float')
    plt.title('График зависимости времени подсчета от длины вектора')
    plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Типы данных', title_fontsize='8')
    ax.set_xlabel('Длина вектора')
    ax.set_ylabel('Время (сек)')
    plt.grid()
    plt.savefig('Vectors.png')


# создание векторов
v_int, v_flt = [], []
a = 10  # Заданный скаляр
avg_int, avg_fl = 0, 0
time_int, time_float = [], []

for _ in range(100, 1001, 100):
    x_int = [randint(5, 10) for i in range(_)]
    y_int = [randint(5, 10) for j in range(_)]
    x_flt = [uniform(5, 10) for g in range(_)]
    y_flt = [uniform(5, 10) for h in range(_)]

    # Время создания вектора z(int)
    start = default_timer()
    v_int.append(vectors(x_int, y_int, a))
    avg_int += (default_timer() - start)
    time_int.append(avg_int)

    # Время создания вектора z(float)
    start = default_timer()
    v_flt.append(vectors(x_flt, y_flt, a))
    avg_fl += (default_timer() - start)
    time_float.append(avg_fl)

plot(v_int, v_flt, time_int, time_float)
