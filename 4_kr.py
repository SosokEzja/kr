from random import randint, uniform
from timeit import default_timer
import matplotlib.pyplot as plt


def multiply(mat1, mat2):  # Функция перемножения матриц
    result = [[sum(a * b for a, b in zip(i, j)) for j in zip(*mat2)] for i in mat1]
    return result


def plot(elements, int, float):  # Построение графика
    fig, ax = plt.subplots()
    plt.plot(elements, int, marker='o', label='int')
    plt.plot(elements, float, marker='o', label='float')
    plt.title('График зависимости времени подсчета от размерности матрицы')
    plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Прямые', title_fontsize='8')
    ax.set_xlabel('Размерность матрицы')
    ax.set_ylabel('Время (сек)')
    plt.grid()
    plt.savefig('Matrix.png')


elem, time_int, time_float = [], [], []
for _ in range(10, 101, 10):  # Создание матриц
    mat_int1 = [[randint(1, 10) for i in range(_)] for j in range(_)]
    mat_int2 = [[randint(1, 10) for i in range(_)] for j in range(_)]
    mat_fl1 = [[uniform(1, 10) for i in range(_)] for j in range(_)]
    mat_fl2 = [[uniform(1, 10) for i in range(_)] for j in range(_)]

    avg_int, avg_fl = 0, 0
    for t in range(3):  # Время перемножения матриц
        start = default_timer()
        n1 = multiply(mat_int1, mat_int2)
        avg_int += default_timer() - start

        start = default_timer()
        n2 = multiply(mat_fl1, mat_fl2)
        avg_fl += default_timer() - start

    # Заносим все в массивы
    elem.append(_ ** 2)
    time_int.append(avg_int / 3)
    time_float.append(avg_fl / 3)
plot(elem, time_int, time_float)
